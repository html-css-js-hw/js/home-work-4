function fetchFilms() {
    fetch('https://ajax.test-danit.com/api/swapi/films')
        .then(response => response.json())
        .then(data => {
            const filmsList = document.getElementById('films-list');
            data.forEach(film => {
                const filmElement = document.createElement('div');
                filmElement.innerHTML = `
            <h2>Episode ${film.episodeId}: ${film.name}</h2>
            <p>${film.openingCrawl}</p>
            <div id="characters-${film.episodeId}">Loading characters... <span class="loading"></span></div>
          `;
                filmsList.appendChild(filmElement);
                fetchCharacters(film.episodeId, film.characters);
            });
        })
        .catch(error => console.error('Error fetching films:', error));
}
function fetchCharacters(episodeId, characterUrls) {
    const charactersElement = document.getElementById(`characters-${episodeId}`);
    Promise.all(characterUrls.map(url => fetch(url).then(response => response.json())))
        .then(characters => {
            charactersElement.innerHTML = '<h3>Characters:</h3>';
            const ul = document.createElement('ul');
            characters.forEach(character => {
                const li = document.createElement('li');
                li.textContent = character.name;
                ul.appendChild(li);
            });
            charactersElement.appendChild(ul);
        })
        .catch(error => console.error(`Error fetching characters for Episode ${episodeId}:`, error));
}

document.addEventListener('DOMContentLoaded', fetchFilms);